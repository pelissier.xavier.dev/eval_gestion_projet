import { Request, Response } from "express-serve-static-core";

export default class ProjetController
{
    /**
     * Affiche la liste des articles
     * @param req 
     * @param res 
     */
    static index(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const projets = db.prepare('SELECT * FROM projets').all();

        res.render('pages/index', {
            title: 'Gestion de Projets',
            projets: projets
        });
    }

    /**
     * Affiche le formulaire de creation d'article
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void
    {
    res.render('pages/projet-create');
    }

    /**
     * Recupere le formulaire et insere l'article en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO projets ("projets_titre", "projets_description", "clients_id_clients") VALUES (?, ?, 1)').run(req.body.title, req.body.content);

        ProjetController.index(req, res);
    }

    /**
     * Affiche 1 article
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const projet = db.prepare('SELECT * FROM projets WHERE id_projets = ?').get(req.params.id);
        
        res.render('pages/projet', {
        projet: projet
        
        });
    }

    /**
     * Affiche le formulaire pour modifier un article
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const projet = db.prepare('SELECT * FROM projets WHERE id_projets = ?').get(req.params.id);
console.log(projet)
        res.render('pages/projet-update', {
            projet: projet
        });
    }   
    
    /**
     * Recupere le formulaire de l'article modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE projets SET projets_titre = ?, projets_description = ? WHERE id_projets = ?').run(req.body.title, req.body.content, req.params.id);

        ProjetController.index(req, res);
    }

    /**
     * Supprime un article
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM projets WHERE id_projets = ?').run(req.params.id);

        ProjetController.index(req, res);
    }
}
