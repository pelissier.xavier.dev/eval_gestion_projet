-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Xavplon
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-21 13:38
-- Created:       2021-12-20 10:21
PRAGMA foreign_keys = OFF;

-- Schema: projets_database
BEGIN;
CREATE TABLE "devs"(
  "id_devs" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "devs_noms" VARCHAR(45) NOT NULL,
  "devs_prenoms" VARCHAR(45) NOT NULL,
  "devs_niveaux" VARCHAR(45) NOT NULL
);
CREATE TABLE "clients"(
  "id_clients" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "clients_name" VARCHAR(45) NOT NULL,
  "clients_adresse" VARCHAR(90) NOT NULL
);
CREATE TABLE "projets"(
  "id_projets" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "projets_description" VARCHAR(255) NOT NULL,
  "projets_titre" VARCHAR(45) NOT NULL,
  "clients_id_clients" INTEGER NOT NULL,
  CONSTRAINT "fk_projets_clients1"
    FOREIGN KEY("clients_id_clients")
    REFERENCES "clients"("id_clients")
);
CREATE INDEX "projets.fk_projets_clients1_idx" ON "projets" ("clients_id_clients");
CREATE TABLE "devs_has_projets"(
  "devs_has_projets_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "devs_id_devs" INTEGER NOT NULL,
  "projets_id_projets" INTEGER NOT NULL,
  CONSTRAINT "fk_devs_has_projets_devs1"
    FOREIGN KEY("devs_id_devs")
    REFERENCES "devs"("id_devs"),
  CONSTRAINT "fk_devs_has_projets_projets1"
    FOREIGN KEY("projets_id_projets")
    REFERENCES "projets"("id_projets")
);
CREATE INDEX "devs_has_projets.fk_devs_has_projets_projets1_idx" ON "devs_has_projets" ("projets_id_projets");
CREATE INDEX "devs_has_projets.fk_devs_has_projets_devs1_idx" ON "devs_has_projets" ("devs_id_devs");
COMMIT;
